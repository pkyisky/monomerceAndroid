package com.cioc.monomerce.startup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cioc.monomerce.R;
import com.cioc.monomerce.backend.BackendServer;
import com.cioc.monomerce.backend.SessionManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;

public class LoginPageActivity extends AppCompatActivity {
    private EditText mobile, mobileOtp;
    private Button loginButton, verifyBtn;
    private TextView goBack;
    private LinearLayout loginForm, otpVerifyForm;
    private TextInputLayout tilMobile, tilOTP;
    private Context mContext;
    private CookieStore httpCookieStore;
    private AsyncHttpClient client;
    private SessionManager sessionManager;
    private String csrfId, sessionId, mobileStr;
    private File file;
    String TAG = LoginPageActivity.class.getName();


    private static final String EMAIL = "email";
    CallbackManager callbackManager;
    private LoginButton loginFacebook;
    private  SignInButton Google_login;


    FirebaseAuth auth;
    GoogleSignInClient signInclient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        mContext = LoginPageActivity.this;
        getSupportActionBar().hide();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        init();
        sessionManager = new SessionManager(this);
        httpCookieStore = new PersistentCookieStore(this);
        httpCookieStore.clear();
        BackendServer server =  new BackendServer(this);
        client = new AsyncHttpClient(true, 80, 443);
        client.setCookieStore(httpCookieStore);
        sessionManager.clearAll();
//        if(!(sessionManager.getCsrfId() == "" && sessionManager.getSessionId() == "")){
//            startActivity(new Intent(this, MainActivity.class));
//            finish();
//        }
        isStoragePermissionGranted();


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();

            }
        });
        callbackManager = CallbackManager.Factory.create();

        Google_login = findViewById(R.id.Googlesign_in_button);
        Google_login.setSize(SignInButton.SIZE_STANDARD);

        loginFacebook = findViewById(R.id.login_fbbutton);
        loginFacebook.setReadPermissions(Arrays.asList("public_profile",EMAIL));



        loginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Toast.makeText(mContext,"success",Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());
                        try {
                            String email  = object.getString("email");
                            String name = object.getString("name");
                            Toast.makeText(mContext,name+"  "+email,Toast.LENGTH_SHORT).show();

                            RequestParams params = new RequestParams();
                            params.put("username", name);
                            params.put("email",email);
                            params.put("password","123");
                            params.put("secretKey","1234");
                            //login/?mode=api&keylogin=1
                            client.post(BackendServer.url+"socialMobileLogin", params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject c) {
                                    Log.e("LoginActivity", "  onSuccess");
                                    Toast.makeText(mContext,"fb login successful",Toast.LENGTH_SHORT).show();
                                    super.onSuccess(statusCode, headers, c);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject c) {
                                    super.onFailure(statusCode, headers, e, c);

                                        Toast.makeText(mContext, "un success"+ e.toString()+"c "+e.toString(), Toast.LENGTH_SHORT).show();
                                        Log.e("LoginActivity", "  onFailure login"+ e.toString()+"c "+e.toString());

                                }

                                @Override
                                public void onFinish() {

                                    logindetails();


                                }
                            });

                           /* client.post(BackendServer.url + "/login/?mode=api&keylogin=1", params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Toast.makeText(mContext,"fb login successful",Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Toast.makeText(mContext,"fb login fail"+error.toString(),Toast.LENGTH_SHORT).show();
                                    Log.e("fb error", error.toString());

                                }
                            });*/



                            } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(mContext,"cancel",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("errror 1 ",error.toString());
                Toast.makeText(mContext,"error 1  :"+error.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        checkLogIn();
       auth=FirebaseAuth.getInstance();

        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestScopes( new Scope("https://www.googleapis.com/auth/gmail.readonly"))
                .requestServerAuthCode(getString(R.string.default_web_client_id)).build();

        signInclient= GoogleSignIn.getClient(this,gso);
        Google_login = findViewById(R.id.Googlesign_in_button);
        Google_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = signInclient.getSignInIntent();
                startActivityForResult(signInIntent,101);
            }
        });


    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();
       // updateUI(currentUser);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 101) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately

                // ...
            }
        }
    }

   private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = auth.getCurrentUser();
                            String token = account.getIdToken();
                            String refreshToken = account.getServerAuthCode();


                            String username = user.getDisplayName();
                            String email = user.getEmail();

                            RequestParams params = new RequestParams();
                            params.put("email", email);
                            params.put("username", username);
                            params.put("password", "123");
                            params.put("secretKey","1234");
                            client.post( BackendServer.url+"socialMobileLogin", params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject c) {
                                    Log.e("LoginActivity", "  onSuccess");
                                    Toast.makeText(mContext,"gmail login successful",Toast.LENGTH_SHORT).show();
                                    super.onSuccess(statusCode, headers, c);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject c) {
                                    super.onFailure(statusCode, headers, e, c);

                                        Toast.makeText(mContext, "un success"+ e.toString()+"c "+e.toString(), Toast.LENGTH_SHORT).show();
                                        Log.e("LoginActivity", "  onFailure login"+e.toString()+"ob "+c.toString() );

                                }

                                @Override
                                public void onFinish() {

                                    logindetails();


                                }
                            });
                            //Toast.makeText(getApplicationContext(),username+" "+email,+Toast.LENGTH_LONG).show();


                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(getApplicationContext(), "User login Failed", Toast.LENGTH_LONG).show();

                        }

                    }
                });





    }




    private void checkLogIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if(isLoggedIn){
            Toast.makeText(getApplicationContext(),"already logged in",Toast.LENGTH_SHORT).show();
        }
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }*/



    void init() {
        mobile = findViewById(R.id.mobile_no);
        tilMobile = findViewById(R.id.til_mobile);
        loginButton = findViewById(R.id.sign_in_button);
        loginForm = findViewById(R.id.login_form);
        otpVerifyForm = findViewById(R.id.otp_verify_layout);
        otpVerifyForm.setVisibility(View.GONE);
        mobileOtp = findViewById(R.id.otpEdit);
        tilOTP = findViewById(R.id.til_otp);
        verifyBtn = findViewById(R.id.verify_button);
        goBack = findViewById(R.id.go_back);
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.PROCESS_OUTGOING_CALLS) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE ,
                        Manifest.permission.READ_PHONE_STATE , Manifest.permission.PROCESS_OUTGOING_CALLS,
                        Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 1; i < 7; i++) {
            if (requestCode == i){
                if (grantResults.length > 0
                        && grantResults[i-1] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Permission: " + permissions[i-1] + "was " + grantResults[i-1]);
                    //resume tasks needing this permission
                }
                return;
            }
        }
    }

    public void newRegistration(View view) {
        Intent i = new Intent(getApplicationContext(), SignUpWithMobileActivity.class);
        startActivity(i);
    }

    public void signUpPage(View view){
        loginForm.setVisibility(View.VISIBLE);
        otpVerifyForm.setVisibility(View.GONE);
    }

    public void login(){
        Toast.makeText(this, BackendServer.url, Toast.LENGTH_LONG).show();
        mobileStr = mobile.getText().toString();
        if (mobileStr.isEmpty()){
            tilMobile.setErrorEnabled(true);
            tilMobile.setError("Mobile no. is required.");
            mobile.requestFocus();
        } else {
            tilMobile.setErrorEnabled(false);
            RequestParams params = new RequestParams();
            params.put("id", mobileStr);

            client.post(mContext,BackendServer.url+"/generateOTP/", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    otpVerifyForm.setVisibility(View.VISIBLE);
                    loginForm.setVisibility(View.GONE);
                    getSmsOTP();
                    verifyBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loginFromOTP(mobileStr);
                        }
                    });
                    Toast.makeText(LoginPageActivity.this, "getting otp", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toast.makeText(LoginPageActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
                    Log.e("login",responseString+"  "+throwable.toString());
                }
            });
        }
    }

    public void loginFromOTP(String mob){
        Toast.makeText(this, BackendServer.url, Toast.LENGTH_LONG).show();
        String mobOtp = mobileOtp.getText().toString();
        if (mobOtp.isEmpty()){
            tilOTP.setErrorEnabled(true);
            tilOTP.setError("Mobile OTP is required.");
            mobileOtp.requestFocus();
        } else {
            tilOTP.setErrorEnabled(false);
            csrfId = sessionManager.getCsrfId();
            sessionId = sessionManager.getSessionId();
            if (csrfId.equals("") && sessionId.equals("")) {
                RequestParams params = new RequestParams();
                params.put("username", mob);
                params.put("otp", mobOtp);
                client.post(BackendServer.url + "/login/?mode=api", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject c) {
                        Log.e("LoginActivity", "  onSuccess");
                        super.onSuccess(statusCode, headers, c);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject c) {
                        super.onFailure(statusCode, headers, e, c);
                        if (statusCode == 401) {
                            Toast.makeText(mContext, "un success", Toast.LENGTH_SHORT).show();
                            Log.e("LoginActivity", "  onFailure login");
                        }
                    }

                    @Override
                    public void onFinish() {

                        logindetails();


                    }
                });
            }
        }
    }

    private void logindetails() {
        List<Cookie> lst = httpCookieStore.getCookies();
        if (lst.isEmpty()) {
            Toast.makeText(mContext, String.format("Error , Empty cookie login page store"), Toast.LENGTH_SHORT).show();
            Log.e("LoginActivity", "Empty cookie store");
        } else {
            if (lst.size() < 2) {
                String msg = String.format("Error while logining, fetal error!");
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("LoginActivity", ""+msg);
                return;
            }

            Cookie csrfCookie = lst.get(0);
            Cookie sessionCookie = lst.get(1);

            String csrf_token = csrfCookie.getValue();
            String session_id = sessionCookie.getValue();
            File dir = new File(Environment.getExternalStorageDirectory() + "/"+getString(R.string.app_name1));
            Log.e("MyAccountActivity", "" + Environment.getExternalStorageDirectory() + "/"+getString(R.string.app_name1));
            if (dir.exists())
                if (dir.isDirectory()) {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(dir, children[i]).delete();
                    }
                    dir.delete();
                }
            file = new File(Environment.getExternalStorageDirectory()+"/"+getString(R.string.app_name1));
            Log.e("directory",""+file.getAbsolutePath());
            if (file.mkdir()) {
                sessionManager.setCsrfId(csrf_token);
                sessionManager.setSessionId(session_id);
                sessionManager.setUsername(mobile.getText().toString());
                Toast.makeText(mContext, "Dir created", Toast.LENGTH_SHORT).show();
                String fileContents = "csrf_token " + sessionManager.getCsrfId() + " session_id " + sessionManager.getSessionId();
                FileOutputStream outputStream;
                try {
                    String path = file.getAbsolutePath() + "/libre.txt";
                    outputStream = new FileOutputStream(path);
                    outputStream.write(fileContents.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("isExternalStorageWritable", "" + mContext.getFilesDir().getAbsoluteFile().getPath());
                startActivity(new Intent(mContext, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            } else {
                Toast.makeText(mContext, "Dir not created", Toast.LENGTH_SHORT).show();
            }
        }
        Log.e("LoginActivity", "  finished");
    }

    private void getSmsOTP() {
        IncomingSMS.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                String otp = parseCode(messageText);
                mobileOtp.setText(otp);
                loginFromOTP(mobileStr);
            }
        });
    }


    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }
}
