package com.cioc.monomerce.startup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cioc.monomerce.R;
import com.cioc.monomerce.payment.PaymentActivity;

public class Sterling_Payment extends AppCompatActivity {
    LinearLayout visa_Layout, master_Layout, credit_Layout, paytm_Layout, phonepay_Layout, visacart,mastercart,creditcart;
    public boolean res =true,res1=true,res2=true;
    ImageView visa_Select, master_Select, credit_Select,paytm_Select, phonepe_Select;
    EditText cardNumber, cardholderName, cvv_number;
    TextView nextStep,back_step;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sterling__payment);
        visa_Layout = findViewById(R.id.layout_Visa);
        master_Layout = findViewById(R.id.layout_Master);
        credit_Layout = findViewById(R.id.layout_Credit);
        paytm_Layout = findViewById(R.id.layout_Paytm);
        phonepay_Layout = findViewById(R.id.layout_Phonepay);
        visacart = findViewById(R.id.visa_Cart);
        mastercart = findViewById(R.id.master_Cart);
        creditcart = findViewById(R.id.credit_Cart);

        visa_Select=findViewById(R.id.visaMark);
        master_Select=findViewById(R.id.masterMark);
        credit_Select= findViewById(R.id.creditMark);
        paytm_Select =findViewById(R.id.paytmMark);
        phonepe_Select =findViewById(R.id.phonepeMark);

        cardNumber = findViewById(R.id.card_Number);
        cardholderName= findViewById(R.id.card_holderName);
        cvv_number = findViewById(R.id.cvvNumber);
        nextStep = findViewById(R.id.next_Step);
        back_step = findViewById(R.id.button_Back);


        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardholderName.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter valid name",Toast.LENGTH_SHORT).show();

                }else if(cardNumber.getText().toString().equals("") || cardNumber.getText().toString().length()!=12){
                    Toast.makeText(getApplicationContext(),"Enter correct card details",Toast.LENGTH_SHORT).show();

                }else if(cvv_number.getText().toString().equals("") || cvv_number.getText().toString().length()!=3){
                    Toast.makeText(getApplicationContext(),"Enter valid CVV details",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(getApplicationContext(),"ok details",Toast.LENGTH_SHORT).show();
                    cvv_number.setText("");
                    cardNumber.setText("");
                    cardholderName.setText("");
                }
            }
        });
        back_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),PaymentActivity.class));
            }
        });




        visa_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mastercart.setVisibility(View.GONE);
                creditcart.setVisibility(View.GONE);
                if(res){
                   visacart.setVisibility(View.VISIBLE);
                   res =false;
                }else{
                    visacart.setVisibility(View.GONE);
                    res=true;
                }
                res1=true;
                res2=true;
                visa_Select.setImageResource(R.drawable.ic_right);
                master_Select.setImageResource(R.drawable.ic_round);
                credit_Select.setImageResource(R.drawable.ic_round);
                paytm_Select.setImageResource(R.drawable.ic_round);
                phonepe_Select.setImageResource(R.drawable.ic_round);
            }
        });

        master_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creditcart.setVisibility(View.GONE);
                visacart.setVisibility(View.GONE);
                if(res1){
                    mastercart.setVisibility(View.VISIBLE);
                    res1 =false;
                }else{
                    mastercart.setVisibility(View.GONE);
                    res1=true;
                }
                res=true;
                res2=true;
                visa_Select.setImageResource(R.drawable.ic_round);
                master_Select.setImageResource(R.drawable.ic_right);
                credit_Select.setImageResource(R.drawable.ic_round);
                paytm_Select.setImageResource(R.drawable.ic_round);
                phonepe_Select.setImageResource(R.drawable.ic_round);
            }
        });

        credit_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mastercart.setVisibility(View.GONE);
                visacart.setVisibility(View.GONE);
                if(res2){
                    creditcart.setVisibility(View.VISIBLE);
                    res2 =false;
                }else{
                    creditcart.setVisibility(View.GONE);
                    res2=true;
                }
                res=true;
                res1=true;
                visa_Select.setImageResource(R.drawable.ic_round);
                master_Select.setImageResource(R.drawable.ic_round);
                credit_Select.setImageResource(R.drawable.ic_right);
                paytm_Select.setImageResource(R.drawable.ic_round);
                phonepe_Select.setImageResource(R.drawable.ic_round);

            }
        });

    }
}
