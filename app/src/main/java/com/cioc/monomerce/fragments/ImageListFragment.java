package com.cioc.monomerce.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.cioc.monomerce.backend.BackendServer;
import com.cioc.monomerce.R;

import com.cioc.monomerce.entites.Cart;
import com.cioc.monomerce.entites.Generic;

import com.cioc.monomerce.entites.ListingParent;
import com.cioc.monomerce.model.CartUpdation;

import com.cioc.monomerce.product.ItemDetailsActivity;
import com.cioc.monomerce.startup.LoginPageActivity;
import com.cioc.monomerce.startup.MainActivity;
import com.loopj.android.http.AsyncHttpClient;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class ImageListFragment extends Fragment {
    //    public static final String STRING_IMAGE_URI = "ImageUri";
//    public static final String STRING_IMAGE_POSITION = "ImagePosition";
    private static MainActivity mActivity;
    TextView moreItems;
    ProgressBar progressBar;
    RecyclerView recyclerViewList;
    public static ArrayList<ListingParent> listingParents;
    public static ArrayList<Cart> cartWishlist;
    AsyncHttpClient client;
    String pk;
    public static   HashMap listingData ;
    public static final String ARG_OBJECT = "object";





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        listingData = new HashMap();
        listingParents = new ArrayList<>();
        BackendServer backendServer = new BackendServer(mActivity);
        client = backendServer.getHTTPClient();
        listingParents = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_recylerview_list, container, false);
        Bundle ar = getArguments();
        clickBtn(v);
        return v;
    }


    public void clickBtn(View view) {
        recyclerViewList = view.findViewById(R.id.recyclerview_list);
        moreItems = view.findViewById(R.id.more_items);
        progressBar = view.findViewById(R.id.progressBar);
        setRecycler(recyclerViewList);
      //  Log.e("setrecycle","cllick btn");
    }

    String fragmentName = "";
    Generic product;

    private void setRecycler(final RecyclerView recyclerView) {

        for (int i = 0; i < MainActivity.generics.size(); i++) {
            Log.e("fragment","fragment");
            int count=0;
              //if (ImageListFragment.this.getArguments().getInt("type") == i+1 ) {
                product = MainActivity.generics.get(i);
                pk = product.getPk();
                listingParents.clear();
                progressBar.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                moreItems.setVisibility(View.GONE);
                if (ImageListFragment.this.getArguments().getString("pk").equals(pk)) {
                    fragmentName = product.getName();
                    getItems(pk);
                    Log.e("fragment",fragmentName+" "+count++);
                    break;
                }
            //}
       }
    }

    public void getItems(String pk) {

        if(listingData.containsKey(pk)){
            // Toast.makeText(mActivity,"key exist "+pk+" "+fragmentName,Toast.LENGTH_SHORT).show();

             JSONArray response = (JSONArray) listingData.get(pk);
             for (int i = 0; i < response.length(); i++) {
                 try {
                     JSONObject object = response.getJSONObject(i);
                     ListingParent parent = new ListingParent(object);
                     listingParents.add(parent);
                     if (listingParents.size() <= 0) {
                         progressBar.setVisibility(View.VISIBLE);
                         moreItems.setVisibility(View.GONE);
                     } else if (listingParents.size() > 0) {
                         recyclerViewList.setVisibility(View.VISIBLE);
                         progressBar.setVisibility(View.GONE);
                         StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                         recyclerViewList.setLayoutManager(layoutManager);
                         CategoriesRecyclerViewAdapter viewAdapter = new CategoriesRecyclerViewAdapter(listingParents, fragmentName);
                         recyclerViewList.setAdapter(viewAdapter);
                         viewAdapter.notifyDataSetChanged();

                     }
                     if (listingParents.size() > 10) {
                         moreItems.setVisibility(View.VISIBLE);
                         recyclerViewList.setVisibility(View.VISIBLE);
                         moreItems.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 getContext().startActivity(new Intent(getContext(), AllItemsShowActivity.class)
                                         .putExtra("pk", pk)
                                         .putExtra("fragmentName", fragmentName.toUpperCase()));
                             }
                         });
                     } else
                         moreItems.setVisibility(View.GONE);

                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
        }

        else{
            client.get(BackendServer.url + "/api/ecommerce/listing/?parent=" + pk + "&recursive=1", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);

                    listingData.put(pk,response);
                    Log.e("size",listingData.size()+"");
                 //e   Toast.makeText(mActivity,"key added "+pk+" "+fragmentName,Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject object = response.getJSONObject(i);
                            ListingParent parent = new ListingParent(object);
                            listingParents.add(parent);
                                if (listingParents.size() <= 0) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    moreItems.setVisibility(View.GONE);
                                } else if (listingParents.size() > 0) {
                                    recyclerViewList.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                                    recyclerViewList.setLayoutManager(layoutManager);
                                    CategoriesRecyclerViewAdapter viewAdapter = new CategoriesRecyclerViewAdapter(listingParents, fragmentName);
                                    recyclerViewList.setAdapter(viewAdapter);
                                    viewAdapter.notifyDataSetChanged();

                                }
                                if (listingParents.size() > 10) {
                                    moreItems.setVisibility(View.VISIBLE);
                                    recyclerViewList.setVisibility(View.VISIBLE);
                                    moreItems.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getContext().startActivity(new Intent(getContext(), AllItemsShowActivity.class)
                                                    .putExtra("pk", pk)
                                                    .putExtra("fragmentName", fragmentName.toUpperCase()));
                                        }
                                    });
                                } else
                                    moreItems.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mActivity, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    public static class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewAdapter.ViewHolder> {
       BackendServer backendServer = new BackendServer(mActivity);
       private ArrayList<ListingParent> mValues;
        CartUpdation cartUpdation = new CartUpdation();
        int remove_add;
        String fname, sku;


        public static class ViewHolder extends RecyclerView.ViewHolder {

            public final Spinner mItem;
            public final ImageView mImageView;
            public final LinearLayout mLayoutItem, mCart2, banner_offer;
            public final ImageView mWishlist;
            TextView itemName, itemPrice, itemDiscount, itemDiscountPrice, itemsQuantity, itemsOut,banner_itemDiscount,commingSoon;
            public  final Button mCartBtn;
            EditText qty;
            Button itemsQuantityAdd,itemsQuantityRemove,qty_btn;
            TextView itemsQuantityNumber, single_qty,quantity_unit;
            LinearLayout layout_add_remove;
            public static ProgressBar sending_Data;

            // TextView spinnerValue;
            boolean res = true;

            ArrayList spinnerlist = new ArrayList();
            String keys[] = {""};
            int ids[] = {android.R.id.text1};

            public ViewHolder(View view) {
                super(view);
                spinnerlist.clear();
               // spinnerValue = view.findViewById(R.id.spinner_value);
                mImageView =  view.findViewById(R.id.image1);
                mLayoutItem = view.findViewById(R.id.layout_item);
                mCart2 = view.findViewById(R.id.layout_action2_cart);
                mWishlist = view.findViewById(R.id.ic_wishlist);
                itemName =  view.findViewById(R.id.item_name);
                mItem =  view.findViewById(R.id.item_variants_spinner);
                itemPrice =  view.findViewById(R.id.item_price);
                itemDiscountPrice =  view.findViewById(R.id.actual_price);
                itemDiscount =  view.findViewById(R.id.discount_percentage);
                itemsQuantity =  view.findViewById(R.id.item_added);
                itemsOut =  view.findViewById(R.id.out_of_stock);
                mCartBtn =  view.findViewById(R.id.card_item_quantity_add);
                itemsQuantityNumber =  view.findViewById(R.id.items_add_quantity);
                itemsQuantityAdd =  view.findViewById(R.id.items_quantity_add);
                itemsQuantityRemove =  view.findViewById(R.id.items_quantity_remove);
                layout_add_remove= view.findViewById(R.id.layout_action1_cart);
                banner_offer = view.findViewById(R.id.offer_layout);
                banner_itemDiscount = view.findViewById(R.id.discount_offer);
                qty= view.findViewById(R.id.qty);
                qty_btn = view.findViewById(R.id.qty_btn);
                single_qty = view.findViewById(R.id.single_qty);
                quantity_unit = view.findViewById(R.id.quantity_Unit);
                sending_Data =view.findViewById(R.id.sending_Data);
               // commingSoon = view.findViewById(R.id.comming_soon);


            }
        }

        public CategoriesRecyclerViewAdapter(ArrayList<ListingParent> items, String name) {
            mValues = items;
            fname = name;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public int getItemCount() {

            return mValues.size()>10 ? 10 : mValues.size();

        }

        @Override
        public int getItemViewType(int position) {
            return super.getItemViewType(position);
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.layout_add_remove.setVisibility(View.GONE);
            final ListingParent parent = mValues.get(position);// to increase and decrease product quantity


            holder.itemsQuantityAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.sending_Data.setVisibility(View.VISIBLE);
                    String quan = holder.itemsQuantityNumber.getText().toString();
                    if(MainActivity.count1==0){
                        sku=parent.getSerialNo();
                    }
                boolean res =   cartUpdation.checkInCart(sku);
                   remove_add = Integer.parseInt(quan);
                    if(remove_add<=12) {
                        remove_add++;
                        holder.itemsQuantityNumber.setText(remove_add + "");
                        holder.itemsQuantityRemove.setVisibility(View.VISIBLE);
                        MainActivity.quantity = remove_add;
                        if(res) {
                        boolean ress = cartUpdation.updateCart(remove_add);
                            if(ress){
                                Toast.makeText(mActivity,"updated",Toast.LENGTH_SHORT).show();
                                holder.sending_Data.setVisibility(View.GONE);
                            }else{
                                holder.sending_Data.setVisibility(View.GONE);
                                Toast.makeText(mActivity,"Server Error..try after Sometime",Toast.LENGTH_SHORT).show();
                            }
                            MainActivity.count1=0;
                       }
                    }else{
                        Toast.makeText(mActivity,"You cannot add more product",Toast.LENGTH_SHORT).show();
                    }
                }
                });


            holder.itemsQuantityRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   holder.sending_Data.setVisibility(View.VISIBLE);
                    String quan = holder.itemsQuantityNumber.getText().toString();
                    remove_add = Integer.parseInt(quan);
                    remove_add =remove_add-1;
                    if(MainActivity.count1==0){
                        sku = parent.getSerialNo();
                    }
                  boolean res =  cartUpdation.checkInCart(sku);
                    if(remove_add<1){
                        holder.layout_add_remove.setVisibility(View.GONE);
                        holder.mCartBtn.setVisibility(View.VISIBLE);
                        holder.mCart2.setVisibility(View.VISIBLE);
                        holder.qty.setVisibility(View.VISIBLE);
                        holder.qty_btn.setVisibility(View.VISIBLE);
                        if(res) {
                          boolean ress =  cartUpdation.deleteCart(CartUpdation.cart_pk,0);
                          if(ress){
                              Toast.makeText(mActivity,"Removed",Toast.LENGTH_SHORT).show();
                              holder.sending_Data.setVisibility(View.GONE);
                          }else{
                              holder.sending_Data.setVisibility(View.GONE);
                              Toast.makeText(mActivity,"Server Error..try after Sometime",Toast.LENGTH_SHORT).show();
                          }
                            MainActivity.count1=0;
                       }
                        holder.itemsQuantityNumber.setText("1");
                    }else{
                        holder.itemsQuantityNumber.setText(remove_add+"");
                        MainActivity.quantity=remove_add;

                    if(res) {
                        MainActivity.count1=0;
                        boolean ress=cartUpdation.updateCart(remove_add);
                        if(ress){
                            Toast.makeText(mActivity,"Updated",Toast.LENGTH_SHORT).show();
                            holder.sending_Data.setVisibility(View.GONE);
                        }else{
                            holder.sending_Data.setVisibility(View.GONE);
                            Toast.makeText(mActivity,"Server Error..try after Sometime",Toast.LENGTH_SHORT).show();
                        }
                    }
                    }
                }
                });

            String link;

    try {
    if (parent.getFilesAttachment().equals("null")) {
        link = BackendServer.url + "/static/images/ecommerce.jpg";
    } else
        link = parent.getFilesAttachment();
    Glide.with(mActivity)
            .load(link)
            .into(holder.mImageView);
    }catch (NullPointerException e){
                e.printStackTrace();
    }

            Double d = Double.parseDouble(parent.getProductPrice());
            final int price = (int) Math.round(d);
            Double d1 = Double.parseDouble(parent.getProductDiscountedPrice());
            final int price1 = (int) Math.round(d1);


                holder.itemName.setText(parent.getProductName());
                int howmuch = (int) Double.parseDouble(parent.getHowMuch());
                String spinnerstr = howmuch + " " + parent.getUnit();
                holder.single_qty.setText(spinnerstr);


                if (parent.getProductDiscount().equals("0")) {
                    holder.itemPrice.setText("\u20B9" + price);
                    spinnerstr += " - \u20B9" + price;
                    holder.itemDiscountPrice.setVisibility(View.GONE);
                    holder.itemDiscount.setVisibility(View.GONE);
                    holder.banner_offer.setVisibility(View.GONE);
                } else {
                    holder.itemPrice.setText("\u20B9" + price1);
                    holder.itemDiscountPrice.setVisibility(View.VISIBLE);
                    holder.itemDiscountPrice.setText("\u20B9" + price);
                    spinnerstr += " - \u20B9" + price1;
                    holder.itemDiscountPrice.setPaintFlags(holder.itemDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.itemDiscount.setVisibility(View.VISIBLE);
                    holder.itemDiscount.setText(parent.getProductDiscount() + "% OFF");
                    holder.banner_offer.setVisibility(View.VISIBLE);
                    holder.banner_itemDiscount.setText(parent.getProductDiscount() + "% OFF");
                }
                sku = parent.getSerialNo();
                HashMap map = new HashMap();
                map.put(holder.keys[0], spinnerstr);
                map.put("sku", parent.getSerialNo());
                map.put("disPer", parent.getProductDiscount());
                map.put("discount", price1);
                holder.spinnerlist.add(map);
           /* }catch (NumberFormatException e){
                e.printStackTrace();
            }*/
            JSONArray array = parent.getItemArray();
            if (array.length()>0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObj = null;
                    try {
                        String strvalue;
                        jsonObj = array.getJSONObject(i);
                        String sku = jsonObj.getString("sku");
                        String updated = jsonObj.getString("updated");
                        String unitPerpack = jsonObj.getString("unitPerpack");
                        String created = jsonObj.getString("created");
                        String pricearray = jsonObj.getString("price");
                        String discountedPrice = jsonObj.getString("discountedPrice");
                        String parent_id = jsonObj.getString("parent_id");
                        String serialId = jsonObj.getString("serialId");
                        String id = jsonObj.getString("id");
                        Double rspoint = Double.parseDouble(pricearray);
                        final int rs = (int) Math.round(rspoint);
                        Double rsPointdis = Double.parseDouble(discountedPrice);
                        final int rsdis = (int) Math.round(rsPointdis);
                        double qty = (int) (Double.parseDouble(parent.getHowMuch())*(Double.parseDouble(unitPerpack)));
                        if(qty>999){
                            double qty1 = (qty/1000);

                            strvalue = qty1 +" "+"Kg"+  " - \u20B9" + rs;
                        }else {
                            strvalue = (int)qty + " " + parent.getUnit() + " - \u20B9" + rs;
                        }
                        HashMap map1 = new HashMap();
                        map1.put(holder.keys[0], strvalue);
                        map1.put("sku", sku);
                        map1.put("disPer", parent.getProductDiscount());
                        map1.put("discount", rsdis);
                        holder.spinnerlist.add(map1);

                       // int size = holder.spinnerlist.size();
               //         Toast.makeText(mActivity,size,Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    catch (NumberFormatException e){
                        e.printStackTrace();
                    }
                }
            }

            holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MainActivity.username.equals("")) {
                        mActivity.startActivity(new Intent(mActivity, LoginPageActivity.class));
                    } else {
                        String itemPrice = holder.itemPrice.getText().toString();
                        Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
                        intent.putExtra("listingLitePk", parent.getPk());

//                        intent.putExtra(STRING_IMAGE_URI, parent.getFilesAttachment());
//                        intent.putExtra(STRING_IMAGE_POSITION, position);
                        mActivity.startActivity(intent);
                        mActivity.overridePendingTransition(R.anim.slide,R.anim.slide_out);
                    }
                }
            });


            SimpleAdapter adapter = new SimpleAdapter(mActivity, holder.spinnerlist, android.R.layout.simple_spinner_dropdown_item, holder.keys,holder.ids);
            holder.mItem.setAdapter(adapter);
            holder.mItem.setSelection(0);


            holder.mItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent1, View view, int position, long id) {
                    // holder.layout_add_remove.setVisibility(View.GONE);
                   //holder.itemsQuantityNumber.setText("1");
                    MainActivity.count1=position;
                    HashMap map = (HashMap) holder.spinnerlist.get(position);
                    String spinnerValue = (String) map.get(holder.keys[0]);
                    sku = (String) map.get("sku");
                   boolean res =cartUpdation.checkInCart(sku);
                        if(res){
                           // holder.itemsQuantity.setVisibility(View.VISIBLE);
                            holder.itemsQuantityNumber.setVisibility(View.VISIBLE);
                            holder.itemsQuantityAdd.setVisibility(View.VISIBLE);
                            holder.itemsQuantityRemove.setVisibility(View.VISIBLE);
                            holder.layout_add_remove.setVisibility(View.VISIBLE);
                            holder.mCartBtn.setVisibility(View.GONE);
                            holder.mCart2.setVisibility(View.GONE);
                            holder.qty.setVisibility(View.GONE);
                            holder.qty_btn.setVisibility(View.GONE);
                            holder.itemsQuantityNumber.setText(CartUpdation.qtty);
                        }else {
                            // Toast.makeText(mActivity, "not matched " , Toast.LENGTH_SHORT).show();
                            holder.mCart2.setVisibility(View.VISIBLE);
                            //holder.itemsQuantity.setVisibility(View.GONE);
                            holder.layout_add_remove.setVisibility(View.GONE);
                            holder.mCartBtn.setVisibility(View.VISIBLE);
                            holder.mCart2.setVisibility(View.VISIBLE);
                            holder.qty.setVisibility(View.VISIBLE);
                            holder.qty_btn.setVisibility(View.VISIBLE);
                        }
                    String disPer = (String) map.get("disPer");
                    int discount = (int) map.get("discount");
                  //  Log.e("on spinner",sku+" "+MainActivity.count1);
                    String arrSplit[] = spinnerValue.split("-");
                    if (disPer.equals("0")) {
                        holder.banner_offer.setVisibility(View.GONE);
                        holder.itemPrice.setText(arrSplit[1]);
                        holder.itemDiscountPrice.setVisibility(View.GONE);
                        holder.itemDiscount.setVisibility(View.GONE);
                    } else {
                        holder.itemPrice.setText("\u20B9" + discount);
                        holder.itemDiscountPrice.setVisibility(View.VISIBLE);
                        holder.itemDiscountPrice.setText("" + arrSplit[1]);
                        holder.itemDiscountPrice.setPaintFlags(holder.itemDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.itemDiscount.setVisibility(View.VISIBLE);
                        holder.itemDiscount.setText(disPer + "% OFF");
                        holder.banner_offer.setVisibility(View.VISIBLE);
                        holder.banner_itemDiscount.setText(disPer + "% OFF");
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {


                }
            });

            if (parent.isInStock()) {
                holder.itemsOut.setVisibility(View.GONE);
                boolean res =cartUpdation.checkInCart(sku);
                if (res){
                    holder.layout_add_remove.setVisibility(View.VISIBLE);
                    holder.mCartBtn.setVisibility(View.GONE);
                    holder.mCart2.setVisibility(View.GONE);
                    holder.qty.setVisibility(View.GONE);
                    holder.qty_btn.setVisibility(View.GONE);
                    holder.itemsQuantityNumber.setText(CartUpdation.qtty);
                }else {
                    holder.mCart2.setVisibility(View.VISIBLE);
                    holder.itemsQuantity.setVisibility(View.GONE);
                    holder.layout_add_remove.setVisibility(View.GONE);
                }
                        holder.mCartBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                holder.sending_Data.setVisibility(View.VISIBLE);
                                if (MainActivity.username.equals("")) {
                                    mActivity.startActivity(new Intent(mActivity, LoginPageActivity.class));
                                } else {
                                    if(MainActivity.count1==0){
                                        sku = parent.getSerialNo();
                                    }
                                    Log.e("sku",sku);
                                    holder.itemsQuantityNumber.setText(holder.qty.getText().toString());
                                    holder.mCart2.setVisibility(View.GONE);
                                    holder.itemsQuantity.setVisibility(View.GONE);
                                    holder.layout_add_remove.setVisibility(View.VISIBLE);
                                    RequestParams params = new RequestParams();
                                    params.put("prodSku",sku);
                                    params.put("product", parent.getPk());
                                    params.put("qty", holder.qty.getText().toString());
                                    params.put("typ", "cart");
                                    params.put("user", MainActivity.userPK);
                                  boolean ress =  cartUpdation.postInCart(params);
                                  if(ress){
                                      Toast.makeText(mActivity,"Added",Toast.LENGTH_SHORT).show();
                                    holder.sending_Data.setVisibility(View.GONE);
                                  }else{
                                      Toast.makeText(mActivity,"Server Error..try after Sometime",Toast.LENGTH_SHORT).show();
                                      holder.sending_Data.setVisibility(View.GONE);
                                  }
                                    MainActivity.count1=0;

                                }
                            }
                        });

            } else {
                // holder.commingSoon.setVisibility(View.VISIBLE);
                holder.banner_offer.setVisibility(View.GONE);
                holder.itemsOut.setVisibility(View.VISIBLE);
                holder.layout_add_remove.setVisibility(View.GONE);
                holder.itemsQuantity.setVisibility(View.GONE);
                holder.mCartBtn.setVisibility(View.GONE);
                holder.mCart2.setVisibility(View.GONE);
                holder.qty.setVisibility(View.GONE);
                holder.qty_btn.setVisibility(View.GONE);
            }


            //Set click action for wishlist
            String quntWish = parent.getAddedWish();
            int qntWishAdd = Integer.parseInt(quntWish);

            if (qntWishAdd==0) {
                holder.mWishlist.setImageResource(R.drawable.ic_favorite_border_green_24dp);
                holder.res = true;
            }else {
                holder.mWishlist.setImageResource(R.drawable.ic_favorite_red_24dp);
                holder.res = false;
            }

            holder.mWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (MainActivity.username.equals("")) {
                        mActivity.startActivity(new Intent(mActivity, LoginPageActivity.class));
                    } else {

                        if (holder.res) {
                            RequestParams params = new RequestParams();
                            params.put("prodSku",parent.getSerialNo());
                            params.put("product", parent.getPk());
                            params.put("qty", "1");
                            params.put("typ", "favourite");
                            params.put("user", MainActivity.userPK);
                            cartUpdation.postWishList(params);
                            holder.mWishlist.setImageResource(R.drawable.ic_favorite_red_24dp);
                            Toast.makeText(mActivity,"Product added to wishList",Toast.LENGTH_SHORT).show();
                            holder.res=false;

                        } else {

                           boolean ress= cartUpdation.checkInWhishlist(parent.getSerialNo());
                           if(ress){
                               cartUpdation.deleteCart(CartUpdation.cart_pk,1);
                               holder.mWishlist.setImageResource(R.drawable.ic_favorite_border_green_24dp);
                               Toast.makeText(mActivity,"Product Removed from wishList",Toast.LENGTH_SHORT).show();
                               holder.res=true;
                           }
                       }
                    }
                }
            });
            // To hide the spinner if it has single spinner value in it.
           if(holder.spinnerlist.size()==1){
               holder.mItem.setVisibility(View.GONE);
               holder.quantity_unit.setVisibility(View.VISIBLE);
               holder.single_qty.setVisibility(View.VISIBLE);
            }
        }
    }
}
