package com.cioc.monomerce.model;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cioc.monomerce.R;
import com.cioc.monomerce.backend.BackendServer;
import com.cioc.monomerce.fragments.ImageListFragment;
import com.cioc.monomerce.notification.NotificationCountSetClass;
import com.cioc.monomerce.options.WishlistActivity;
import com.cioc.monomerce.startup.MainActivity;
import com.cioc.monomerce.startup.SplashActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

public class CartUpdation {
    boolean res =false;
    public static  String qtty;
    public static String cart_pk="";
    AsyncHttpClient client =new AsyncHttpClient();

    public  boolean checkInCart(String sku){
          boolean res =false;
          for(int i=0;i<MainActivity.cartList.size();i++) {
              String serialNo = MainActivity.cartList.get(i).getProdSku();
              qtty = MainActivity.cartList.get(i).getQuantity();
              cart_pk = MainActivity.cartList.get(i).getPk();
              if(serialNo.equals(sku)) {
                 res =true;
               //  Log.e("checkin cart",cart_pk+" "+qtty+" "+res);
                 break;
            }
            else {
                 res = false;
            }
        }
        return  res;
    }



    public  boolean deleteCart(String pk,int status){
        res =true;
        client.delete(null, BackendServer.url + "/api/ecommerce/cart/"+ pk+"/", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
             //   Log.e("delete success","deleted");
                if(status==0) {
                    MainActivity.notificationCountCart--;
                }
                GetCart.cartItems();
                res = true;
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
              //  Log.e("failure","failure");
                res= false;
            }
        });
        return  res;
    }



    public boolean updateCart(int qty){
        res =true;
        RequestParams params = new RequestParams();
        params.put("qty",qty);
        client.patch(BackendServer.url + "/api/ecommerce/cart/"+cart_pk+"/", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
               // Log.e("success","upadated");
              //  GetCart.cartItems();
                res= true;
                }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
              //  Log.e("failure","not updated"+error);
                res = false;
            }
        });
        return res;
        }



        public boolean postInCart(RequestParams params){
            res =true;
            client.post(BackendServer.url+"/api/ecommerce/cart/", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                 //   Log.e("added ","added");
                    MainActivity.notificationCountCart++;
                    GetCart.cartItems();
                    res = true;
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                   // Log.e("failed ","failed");
                    res = false;
                }
            });
            return  res;
        }

        public boolean checkInWhishlist(String sku){
            res =true;
            for(int i = 0; i<WishlistActivity.wishList.size(); i++) {
                String serialNo = WishlistActivity.wishList.get(i).getProdSku();
                qtty =WishlistActivity.wishList.get(i).getQuantity();
                cart_pk =WishlistActivity.wishList.get(i).getPk();
                if(serialNo.equals(sku)) {
                    res =true;
                   // Log.e("checkin cart",cart_pk+" "+qtty+" "+res);
                    break;
                }
                else {
                    res = false;
                }
            }
            return  res;
        }


        public void postWishList(RequestParams params){

            client.post(BackendServer.url + "/api/ecommerce/cart/", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                   // Log.e("added wishList", "wishlist added");
                    GetCart.getWishListItem();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                  //  Log.e("on failure",error.toString());
                }
            });
        }


}
