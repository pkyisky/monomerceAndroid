package com.cioc.monomerce.model;


import com.cioc.monomerce.entites.ListingParent;

import java.util.Comparator;

class SortGenericProduct implements Comparator<ListingParent>{

    @Override
    public int compare(ListingParent o1, ListingParent o2) {
        return o1.getProductPrice().compareTo(o2.getProductPrice());
    }
}
