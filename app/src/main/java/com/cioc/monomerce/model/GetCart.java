package com.cioc.monomerce.model;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cioc.monomerce.backend.BackendServer;
import com.cioc.monomerce.entites.Address;
import com.cioc.monomerce.entites.Cart;
import com.cioc.monomerce.options.NewAddressActivity;
import com.cioc.monomerce.options.WishlistActivity;
import com.cioc.monomerce.startup.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class GetCart {
    public  static ArrayList cartList= new ArrayList();
    public  static  ArrayList wishList = new ArrayList();
    static AsyncHttpClient client = new AsyncHttpClient();

    public static void cartItems(){
        client.get(BackendServer.url + "/api/ecommerce/cart/?&typ=cart&user=" + MainActivity.userPK,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Cart cart = new Cart(object);
                                cartList.add(cart);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        MainActivity.cartList=cartList;
                        Log.e("updated cart", "updated cart" );
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray
                            errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }
                });

    }

    public static  void  getWishListItem(){
        client.get(BackendServer.url+"/api/ecommerce/cart/?&Name__contains=&typ=favourite&user="+MainActivity.userPK,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Cart cart = new Cart(object);
                                wishList.add(cart);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        WishlistActivity.wishList=wishList;
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("Error "+statusCode, "onFailure2");
                    }
                });

    }



}
