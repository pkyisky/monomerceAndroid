package com.cioc.monomerce.options;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cioc.monomerce.R;
import com.cioc.monomerce.backend.BackendServer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

public class FeedBackActivity extends AppCompatActivity {

    EditText email, mobile, message;
    TextInputLayout tilEmail, tilMobile, tilMessage;
    Button feedbackBtn;
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tilEmail = findViewById(R.id.til_email);
        tilMobile = findViewById(R.id.til_mobile);
        tilMessage = findViewById(R.id.til_message);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        message = findViewById(R.id.message);
        feedbackBtn = findViewById(R.id.feedback_button);
        BackendServer backend = new BackendServer(this);
        client = backend.getHTTPClient();

        feedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strEmail = email.getText().toString();
                String strMobile = mobile.getText().toString();
                String strMessage = message.getText().toString();
                if (strEmail.isEmpty()){
                    tilEmail.setErrorEnabled(true);
                    tilEmail.setError("Email-id is required.");
                    email.requestFocus();
                } else {
                    tilEmail.setErrorEnabled(false);
                    if (strMobile.isEmpty()){
                        tilMobile.setErrorEnabled(true);
                        tilMobile.setError("Mobile no is required.");
                        mobile.requestFocus();
                    } else {
                        tilMobile.setErrorEnabled(false);
                        if(strMessage.isEmpty()){
                            tilMessage.setErrorEnabled(true);
                            tilMessage.setError("Please type your message here..");
                            message.requestFocus();
                        }else{
                            RequestParams params = new RequestParams();
                            params.put("email",strEmail);
                            params.put("mobile",strMobile);
                            params.put("message",strMessage);
                            client.post(BackendServer.url + "/api/ecommerce/supportFeed/", params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Toast.makeText(getApplicationContext()," Saved  Sucessfully",Toast.LENGTH_SHORT).show();
                                    email.setText("");
                                    mobile.setText("");
                                    message.setText("");

                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Toast.makeText(getApplicationContext(),"failure"+error,Toast.LENGTH_SHORT).show();
                                }
                            });


                        }
                    }
                }

                }


        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
