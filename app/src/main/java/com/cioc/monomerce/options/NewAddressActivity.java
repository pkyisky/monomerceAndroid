package com.cioc.monomerce.options;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cioc.monomerce.backend.BackendServer;
import com.cioc.monomerce.R;
import com.cioc.monomerce.entites.Address;
import com.cioc.monomerce.model.GetCart;
import com.cioc.monomerce.payment.PaymentActivity;
import com.cioc.monomerce.startup.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class NewAddressActivity extends AppCompatActivity {
    public Context mContext;
    EditText city, street, landMark, state, pincode, country, name, mobile;
    TextView savedAdd, cityErr, streetErr, stateErr, pincodeErr, landMarkErr, countryErr, nameErr, mobileErr, saveLayoutAction, continuePayment,newAddressBtn, address_number ;
    AsyncHttpClient client;
    CheckBox primaryAdd;
    RecyclerView recyclerViewAddress;
    String pincity="";
    String pinstate="";
    LinearLayout newAddresslayout, save_new_Address;
    ProgressBar load_Address;
    String pin;
    TextView update_Address;
    String address_pk="";
   public static ArrayList<Address> addressList,pinCityState;
    public boolean res = true;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_address);
        mContext = NewAddressActivity.this;
        BackendServer backend = new BackendServer(NewAddressActivity.this);
        client = backend.getHTTPClient();
        load_Address = findViewById(R.id.load_address);

      //  client = new AsyncHttpClient();

       // getSupportActionBar().hide();
        String s = getIntent().getStringExtra("newAdd");

        init();
        if (!(s==null)){
            addressList = new ArrayList<>();
            getAddress();
            newAddressBtn.setVisibility(View.VISIBLE);
            savedAdd.setVisibility(View.VISIBLE);
            address_number.setVisibility(View.VISIBLE);
            continuePayment.setVisibility(View.GONE);
            recyclerViewAddress.setVisibility(View.VISIBLE);
            save_new_Address.setVisibility(View.GONE);
            newAddresslayout.setVisibility(View.GONE);
        }

        update_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAdress(address_pk);
            }
        });

        continuePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(true);
            }
        });

       saveLayoutAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(false);
                }
           });


       newAddressBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(res) {
                   newAddresslayout.setVisibility(View.VISIBLE);
                   save_new_Address.setVisibility(View.VISIBLE);
                   newAddressBtn.setText("Hide Address");
                   recyclerViewAddress.setVisibility(View.GONE);
                   savedAdd.setVisibility(View.GONE);
                   address_number.setVisibility(View.GONE);
                   newAddressBtn.setVisibility(View.GONE);
                   load_Address.setVisibility(View.GONE);
                   res=false;
               }else{
                   res=true;
                   newAddresslayout.setVisibility(View.GONE);
                   save_new_Address.setVisibility(View.GONE);
                   newAddressBtn.setText("Add New Address");
                   recyclerViewAddress.setVisibility(View.VISIBLE);
                   savedAdd.setVisibility(View.VISIBLE);
                   address_number.setVisibility(View.VISIBLE);
                   newAddressBtn.setVisibility(View.VISIBLE);
               }
           }
       });


         pincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
             @Override
             public void onFocusChange(View v, boolean hasFocus) {
             pin = pincode.getText().toString();
                 getCityState();
             }
         });
         mobile.setText(MainActivity.username);


       }


       public void updateAdress (String pk ){
           String streetStr = street.getText().toString().trim();
           String landMarkStr = landMark.getText().toString().trim();
           String cityStr = city.getText().toString().trim();
           String stateStr = state.getText().toString().trim();
           String pincodeStr = pincode.getText().toString().trim();
           String countryStr = country.getText().toString().trim();
           String nameStr = name.getText().toString().trim();
           String mobStr = mobile.getText().toString().trim();

           if (cityStr.isEmpty()){
               cityErr.setVisibility(View.VISIBLE);
               cityErr.setText("Please provide the necessary details.");
           } else {
               cityErr.setVisibility(View.GONE);
           }

           if (stateStr.isEmpty()){
               streetErr.setVisibility(View.VISIBLE);
               streetErr.setText("Please provide the necessary details.");
           } else {
               streetErr.setVisibility(View.GONE);
           }

           if (streetStr.isEmpty()){
               streetErr.setVisibility(View.VISIBLE);
               streetErr.setText("Please provide the necessary details.");
           } else {
               streetErr.setVisibility(View.GONE);
           }

           if (pincodeStr.isEmpty()){
               pincodeErr.setVisibility(View.VISIBLE);
               pincodeErr.setText("Please provide the necessary details.");
           } else {
               pincodeErr.setVisibility(View.GONE);
           }
           if (countryStr.isEmpty()){
               countryErr.setVisibility(View.VISIBLE);
               countryErr.setText("Please provide the necessary details.");
           } else {
               countryErr.setVisibility(View.GONE);
           }
           if (nameStr.isEmpty()){
               nameErr.setVisibility(View.VISIBLE);
               nameErr.setText("Please provide the necessary details.");
           } else {
               nameErr.setVisibility(View.GONE);
           }
           if (mobStr.isEmpty()){
               mobileErr.setVisibility(View.VISIBLE);
               mobileErr.setText("Please provide the necessary details.");
           } else {
               mobileErr.setVisibility(View.GONE);
           }
           if (streetStr.length()==0) {
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               street.requestFocus();
               return;
           }if (cityStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               city.requestFocus();
               return;
           }if (pincodeStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               pincode.requestFocus();
               return;
           }if (stateStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               state.requestFocus();
               return;
           }if (countryStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               country.requestFocus();
               return;
           }if (nameStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               name.requestFocus();
               return;
           }if (mobStr.length()==0){
               Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
               mobile.requestFocus();
               return;
           } boolean primary;
           if(primaryAdd.isChecked()){
               primary = true;
           }else{
               primary =false;
           }
           RequestParams params = new RequestParams();

           params.put("user", MainActivity.userPK);
           params.put("title", nameStr);
           params.put("street", streetStr);
           params.put("city", cityStr);
           params.put("state", stateStr);
           params.put("pincode", pincodeStr);
           params.put("lat", "");
           params.put("lon", "");
           params.put("country", countryStr);
           params.put("landMark", landMarkStr);
           params.put("mobileNo", mobStr);
           params.put("primary", primary );

           client.patch(BackendServer.url + "/api/ecommerce/address/" + pk, params, new AsyncHttpResponseHandler() {
               @Override
               public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                   Toast.makeText(mContext,"success",Toast.LENGTH_SHORT).show();
                   startActivity(new Intent(NewAddressActivity.this, MyAccountActivity.class));
                   overridePendingTransition(R.anim.slide_out,R.anim.slide);
               }

               @Override
               public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                   Toast.makeText(mContext,"failed"+error+" "+statusCode,Toast.LENGTH_SHORT).show();
               }
           });



       }

    public void save(final boolean res) {

        String streetStr = street.getText().toString().trim();
        String landMarkStr = landMark.getText().toString().trim();
        String cityStr = city.getText().toString().trim();
        String stateStr = state.getText().toString().trim();
        String pincodeStr = pincode.getText().toString().trim();
        String countryStr = country.getText().toString().trim();
        String nameStr = name.getText().toString().trim();
        String mobStr = mobile.getText().toString().trim();

        if (cityStr.isEmpty()){
            cityErr.setVisibility(View.VISIBLE);
            cityErr.setText("Please provide the necessary details.");
        } else {
            cityErr.setVisibility(View.GONE);
        }

        if (stateStr.isEmpty()){
            streetErr.setVisibility(View.VISIBLE);
            streetErr.setText("Please provide the necessary details.");
        } else {
            streetErr.setVisibility(View.GONE);
        }

        if (streetStr.isEmpty()){
            streetErr.setVisibility(View.VISIBLE);
            streetErr.setText("Please provide the necessary details.");
        } else {
            streetErr.setVisibility(View.GONE);
        }

        if (pincodeStr.isEmpty()){
            pincodeErr.setVisibility(View.VISIBLE);
            pincodeErr.setText("Please provide the necessary details.");
        } else {
            pincodeErr.setVisibility(View.GONE);
        }
        if (countryStr.isEmpty()){
            countryErr.setVisibility(View.VISIBLE);
            countryErr.setText("Please provide the necessary details.");
        } else {
            countryErr.setVisibility(View.GONE);
        }
        if (nameStr.isEmpty()){
            nameErr.setVisibility(View.VISIBLE);
            nameErr.setText("Please provide the necessary details.");
        } else {
            nameErr.setVisibility(View.GONE);
        }
        if (mobStr.isEmpty()){
            mobileErr.setVisibility(View.VISIBLE);
            mobileErr.setText("Please provide the necessary details.");
        } else {
            mobileErr.setVisibility(View.GONE);
        }
        if (streetStr.length()==0) {
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            street.requestFocus();
            return;
        }if (cityStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            city.requestFocus();
            return;
        }if (pincodeStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            pincode.requestFocus();
            return;
        }if (stateStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            state.requestFocus();
            return;
        }if (countryStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            country.requestFocus();
            return;
        }if (nameStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            name.requestFocus();
            return;
        }if (mobStr.length()==0){
            Toast.makeText(getApplicationContext(), "Please provide the necessary details.", Toast.LENGTH_SHORT).show();
            mobile.requestFocus();
            return;
        } boolean primary;
        if(primaryAdd.isChecked()){
           primary = true;
        }else{
            primary =false;
        }
        RequestParams params = new RequestParams();

        params.put("user", MainActivity.userPK);
        params.put("title", nameStr);
        params.put("street", streetStr);
        params.put("city", cityStr);
        params.put("state", stateStr);
        params.put("pincode", pincodeStr);
        params.put("lat", "");
        params.put("lon", "");
        params.put("country", countryStr);
        params.put("landMark", landMarkStr);
        params.put("mobileNo", mobStr);
        params.put("primary", primary );

            client.post(BackendServer.url + "/api/ecommerce/address/", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    try {

                        Toast.makeText(getApplicationContext(), params.toString(), Toast.LENGTH_SHORT).show();
                        String pk = response.getString("pk");
                        String title = response.getString("title");
                        String city = response.getString("city");
                        String landMark = response.getString("landMark");
                        String street = response.getString("street");
                        String pincode = response.getString("pincode");
                        String mobile = response.getString("mobileNo");
                        String state = response.getString("state");
                        boolean primary = response.getBoolean("primary");
                        String country = response.getString("country");
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();

                        String addressStr = title + "\n" + street + "\n" + landMark + "\n" + city + ", " + state + "\n " + pincode + ", " + country + "\n\n " + mobile;

                        if (res) {
                            startActivity(new Intent(mContext, PaymentActivity.class)
                                    .putExtra("pk", pk)
                                    .putExtra("totalPrice", getIntent().getStringExtra("totalPrice"))
                                    .putExtra("address", addressStr));
                            Toast.makeText(getApplicationContext(), "res true" + addressStr, Toast.LENGTH_SHORT).show();
                        } else {
                            finish();
                            Toast.makeText(getApplicationContext(), "Saved" + addressStr, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Toast.makeText(getApplicationContext(), "catch error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(getApplicationContext(), "on failure" + errorResponse, Toast.LENGTH_LONG).show();
                }
            });

    }


    public void init() {
        savedAdd = findViewById(R.id.saved_addresses);
        street = findViewById(R.id.address_street);
        city = findViewById(R.id.address_city);
        landMark = findViewById(R.id.address_landmark);
        state = findViewById(R.id.address_state);
        pincode = findViewById(R.id.address_pincode);
        country = findViewById(R.id.address_country);
        name = findViewById(R.id.address_name);
        mobile = findViewById(R.id.address_mob);
        primaryAdd = findViewById(R.id.primary_address);
        saveLayoutAction = findViewById(R.id.add_new_address);
        continuePayment = findViewById(R.id.continue_payment);
        streetErr = findViewById(R.id.streetErrTxt);
        landMarkErr = findViewById(R.id.landmarkErrTxt);
        cityErr = findViewById(R.id.cityErrTxt);
        stateErr = findViewById(R.id.stateErrTxt);
        pincodeErr = findViewById(R.id.pincodeErrTxt);
        countryErr = findViewById(R.id.countryErrTxt);
        nameErr = findViewById(R.id.nameErrTxt);
        mobileErr = findViewById(R.id.mobileErrTxt);
        recyclerViewAddress = findViewById(R.id.recycler_view_address);
        recyclerViewAddress.setVisibility(View.GONE);
        newAddresslayout = findViewById(R.id.new_address_layout);
        newAddressBtn = findViewById(R.id.new_address_btn);
        save_new_Address= findViewById(R.id.save_new_address);
        address_number = findViewById(R.id.address_number);
        update_Address = findViewById(R.id.update_address_btn);
    }


    public void getAddress() {
        load_Address.setVisibility(View.VISIBLE );
        if(MainActivity.userPK == null){
            Toast.makeText(mContext,"pls add address",Toast.LENGTH_SHORT).show();

        }else {
            client.get(BackendServer.url + "/api/ecommerce/address/?user=" + MainActivity.userPK, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    load_Address.setVisibility(View.GONE);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = null;
                        try {
                            object = response.getJSONObject(i);
                            Address address = new Address(object);
                            addressList.add(address);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (addressList.size() > 0) {
                        savedAdd.setVisibility(View.VISIBLE);

                        address_number.setText(addressList.size()+" Address Saved");
                    }else{

                        address_number.setText("No Address Saved");
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    recyclerViewAddress.setLayoutManager(linearLayoutManager);
                    recyclerViewAddress.setAdapter(new AddressAdapter(addressList));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });
        }
    }
    public void getCityState(){

        if(pin.length()<6){
            if(pin.length()==0){
               pincodeErr.setVisibility(View.GONE);
            }else {
                pincodeErr.setVisibility(View.VISIBLE);
                pincodeErr.setText("Please provide valid pin.");
                city.setText("");
                state.setText("");
                country.setText("");
            }

        }else {
            client.get(BackendServer.url + "api/ecommerce/genericPincode/?pincode=" + pin, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                     pincodeErr.setVisibility(View.GONE);
                     JSONObject object =null;
                    try {
                        object = response.getJSONObject(0);
                        pincity = object.getString("city");
                        pinstate = object.getString("state");
                        city.setText(pincity);
                        state.setText(pinstate);
                        country.setText("INDIA");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(getApplicationContext(), "no data" + errorResponse, Toast.LENGTH_LONG).show();
                    city.setText("");
                    state.setText("");
                    country.setText("");
                }


            });
        }
    }



    private class AddressAdapter extends RecyclerView.Adapter<MyViewHolder> {
        ArrayList<Address> addresses;
        int pos;

        public AddressAdapter(ArrayList<Address> addressList) {
            this.addresses = addressList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_address_list, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Address address = addresses.get(position);
           // holder.addresstxt.setText(address.getTitle()+"\n"+address.getStreet()+"\n"+
                  //  address.getLandMark()+"\n"+address.getCity()+", "+address.getState()+address.getCountry()+" \n"+address.getPincode()+"\n\n "+address.getMobile());
            holder.addresstxt.setText(address.getTitle().toUpperCase()+" | "+address.getMobile()+"\n"+address.getStreet()+"\n"+address.getLandMark()+"\n"+address.getCity()+" - "+address.getState()+"\n"+
            address.getCountry()+" - "+address.getPincode());
            holder.delAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    deleteAdd(address.getPk(),position);
                    notifyItemRemoved(position);
                }

            });

            holder.edit_Address.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save_new_Address.setVisibility(View.GONE);
                    update_Address.setVisibility(View.VISIBLE);
                    address_pk = address.getPk();
                    saveLayoutAction.setVisibility(View.GONE);
                    continuePayment.setVisibility(View.GONE);
                  //  save(false, address.getPk());
                    if(res) {
                        newAddresslayout.setVisibility(View.VISIBLE);

                        newAddressBtn.setText("Hide Address");
                        recyclerViewAddress.setVisibility(View.GONE);
                        savedAdd.setVisibility(View.GONE);
                        address_number.setVisibility(View.GONE);
                        newAddressBtn.setVisibility(View.GONE);
                        load_Address.setVisibility(View.GONE);
                        res=false;
                    }else{
                        res=true;
                        newAddresslayout.setVisibility(View.GONE);
                        save_new_Address.setVisibility(View.GONE);
                        newAddressBtn.setText("Add New Address");
                        recyclerViewAddress.setVisibility(View.VISIBLE);
                        savedAdd.setVisibility(View.VISIBLE);
                        address_number.setVisibility(View.VISIBLE);
                        newAddressBtn.setVisibility(View.VISIBLE);
                    }
                    street.setText(address.getStreet());
                    landMark.setText(address.getLandMark());
                    city.setText(address.getCity());
                    country.setText(address.getCountry());
                    pincode.setText(address.getPincode());
                    name.setText(address.getTitle());
                    mobile.setText(address.getMobile());
                    state.setText(address.getState());
                }
            });
        }

        @Override
        public int getItemCount() {
            return addresses.size();
        }

        public void deleteAdd(String pk, int pos){
            client.delete(BackendServer.url + "/api/ecommerce/address/" + pk + "/", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Toast.makeText(NewAddressActivity.this, "deleted", Toast.LENGTH_SHORT).show();
                    addressList.remove(pos);
                  /*  mContext.startActivity(new Intent(mContext, NewAddressActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("newAdd", "set" ));*/

                    notifyItemRemoved(pos);
                    notifyItemRangeChanged(pos,addressList.size());
                    notifyDataSetChanged();
                    if (addressList.size() > 0) {
                        savedAdd.setVisibility(View.VISIBLE);
                        address_number.setText(addressList.size()+" Address Saved");
                    }else{
                        address_number.setText("No Address Saved");
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(NewAddressActivity.this, "deleting failure", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        TextView addresstxt,edit_Address;
        ImageView delAdd;
        LinearLayout deliveryAction;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            addresstxt =  itemView.findViewById(R.id.address_text);
            deliveryAction =  itemView.findViewById(R.id.delivery_action);
            delAdd =  itemView.findViewById(R.id.delete_address);
            deliveryAction.setVisibility(View.GONE);
            edit_Address = itemView.findViewById(R.id.edit_Address);
        }
    }
}
